const _ = require('lodash');
const morgan = require('morgan');
const mongoose = require('mongoose');
const express = require('express');
const app = express();

//MongoURI
const MongoURI =
  'mongodb+srv://Hasan:1998@clusterhasan-2kqza.mongodb.net/BlogInfo?retryWrites=true&w=majority';
mongoose
  .connect(MongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((result) => console.log('Mongo connected'))
  .catch((err) => console.log(err));

//require Blog Model
const { result } = require('lodash');

//register view engine
app.set('view engine', 'ejs');
app.listen(3000);

//morgan middleware
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.get('/', (req, res) => {
  res.redirect('/blogs');
});
app.get('/about', (req, res) => {
  res.render('about', { title: 'About' });
});

//redirect
app.get('/about-us', (req, res) => {
  res.redirect('about', { title: 'About' });
});


//blogs route
app.use('/blogs',require('./routes/blogRoutes'))

app.use((req, res) => {
  res.status(404).render('404', { title: '404' });
});
