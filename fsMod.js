const fs = require('fs');

//read file asynchronously
const myFun = () => {
  fs.readFile('./hello.txt', (err, data) => {
    if (err) console.log(err);

    console.log(data.toString());
  });
};

const d = 'I am the king of the universe';

//write a file asynchronously
fs.writeFile('./hello.txt', d, () => {
  console.log('File was written');
  myFun();
});

for (let i = 0; i < 10; i++) {
  if (!fs.existsSync(`./New${i}`)) {
    fs.mkdir(`./New${i}`, (err) => {
      if (err) console.log(err);
      console.log('Folder created');
    });
  } else {
    console.log('Folder exists');
  }
}

//delete a file
if (fs.existsSync('./New9')) {
  fs.unlink('./hello.txt', (err) => {
    if (err) console.log(err);
    console.log('File deleted');
  });
}
