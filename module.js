const getName = (name) => console.log(name);
const getAge = (age) => console.log(age);

module.exports = { getName, getAge };

const os = require('os');

console.log(os.cpus());
console.log(os.hostname());
console.log(os.homedir())
