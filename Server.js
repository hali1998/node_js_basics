const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
  console.log('Request has been made');
  res.setHeader('Content-Type', 'text/html');
  let path = './views/';
  console.log(req.url);

  switch (req.url) {
    case '/':
      path += 'index.html';
      break;
    case '/about':
      path += 'about.html';
      break;
    case '/about-me':
        res.setHeader('Location','/about');
        res.statusCode=301;
        res.end();
        break;
    default:
      path += '404.html';
      res.statusCode=404;
      break;
  }

  console.log(`Path is ${path}`)
  fs.readFile(path, (err, data) => {
    if (err) {
      console.log(err);
      res.end();
    } else {
      res.end(data);
    }
  });
});

server.listen(3000, 'localhost', () => {
  console.log('Server running on port 3000');
});
