const express=require('express');
const router=express.Router();
const Blog=require('../models/blog')
const {blog_index,blog_details,blog_create_get,blog_create_post,blog_delete} =require('../controllers/blogControllers')


router.get('/', blog_index )
  
  //blog post
  router.post('/', blog_create_post)
  
 
  router.get('/create', blog_create_get)
  
  //single blog
  router.get('/:id', blog_details);
  
  //delete blog
  router.delete('/:id',blog_delete)

  module.exports=router;
  